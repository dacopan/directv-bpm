var gulp = require('gulp');
var plugins = require('gulp-load-plugins')({lazy: false});
var rimraf = require('rimraf');



var paths = {
    dist: {
        css: './src/main/webapp/resources/css/',
        js: './src/main/webapp/resources/js/'
    },
    custom: {
        less: ['./src/main/webapp/assets/less/theme.less'],
        js: [
            './src/main/webapp/assets/js/widgets/*.js',
            './src/main/webapp/assets/js/custom.js'
        ]
    },
    vendor: {
        css: [
            './src/main/webapp/assets/css/bootstrap.min.css',
            './src/main/webapp/assets/css/mdb_pro.css',
            './src/main/webapp/assets/css/font-awesome.css'
        ],
        js: [
            './src/main/webapp/assets/js/bootstrap.min.js',
            './src/main/webapp/assets/js/mdb_pro.js',
            './bower_components/jquery_lazyload/jquery.lazyload.js',
            //'./bower_components/smoothscroll-for-websites/SmoothScroll.js',
            //'./bower_components/isotope/dist/isotope.pkgd.min.js',
            //'./bower_components/isotope-fit-columns/fit-columns.js'
        ]

    }
};

/**
 * removes css- and js-dist folder.
 */
gulp.task('clean', function () {
    //rimraf.sync(paths.dist.css + '../primefaces-dtic_theme/theme.css');
    rimraf.sync(paths.dist.js + 'custom.js');
});

/**
 * compiles less files into css.
 */
gulp.task('custom-less', function () {
    gulp.src(paths.custom.less)
            .pipe(plugins.concat('theme.css'))
            .pipe(plugins.less())
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(paths.dist.css + '../primefaces-dtic_theme/'));
});


gulp.task('custom-js', function () {
    gulp.src(paths.custom.js)
            .pipe(plugins.concat('custom.js'))
            .pipe(plugins.uglify({mangle: false}))
            .pipe(gulp.dest(paths.dist.js));
});

/*gulp.task('fonts', function () {
 gulp.src(paths.vendor.fonts)
 .pipe(gulp.dest(paths.dist.fonts));
 
 });*/
/*
 gulp.task('html', function () {
 var opts = {
 conditionals: true,
 };
 return gulp.src(paths.html)
 .pipe(plugins.minifyHtml(opts))
 .pipe(gulp.dest(paths.dist.html));
 });
 */

/**
 * copies vendor specific files to the public folder.
 */
gulp.task('vendor', function () {
    gulp.src(paths.vendor.css)
            .pipe(plugins.concat('vendor.css'))
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(paths.dist.css));

    gulp.src(paths.vendor.js)
            .pipe(plugins.concat('vendor.js'))
            .pipe(plugins.uglify({mangle: false}))
            .pipe(gulp.dest(paths.dist.js));

});

gulp.task('custom-tinymce', function () {
    gulp.src('./src/main/webapp/assets/less/tinymce.less')
            .pipe(plugins.concat('content.min.css'))
            .pipe(plugins.less())
            .pipe(plugins.minifyCss())
            .pipe(gulp.dest(paths.dist.css + "../tinymce/skins/lightgray/"));
});

gulp.task('custom', ['clean', 'custom-less', 'custom-js']);
gulp.task('all', ['custom', 'vendor']);
gulp.task('default', ['custom']);
