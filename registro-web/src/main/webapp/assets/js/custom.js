/* 
 * Copyright (C) 2015 dacopan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


function showBusy(btn) {
    if (btn) {
        PF(btn).jq.prop('disabled', true);
    }
    console.log('showBusy')

}
function hideBusy(btn) {
    if (btn) {
        PF(btn).jq.prop('disabled', false);
    }
    console.log('hideBusy')

}
function showError(e) {
    if (e) {
    } else {
        showToast('error', 'Error', 'Ha ocurrido un error, ejecutando la acción solicitada.');
        showToast('error', 'Error', 'Refresque la página, si el error persiste contacte con soporte.');
    }
}
function ajaxStatusHandler(xhr, status, args) {
    console.log(xhr);
    console.log(status);
    console.log(args);
}
function onAjaxError(xhr, status, error, btn) {
    if (xhr) {
        console.log(xhr);
        console.log(status);
        console.log(error);
        showError();
    }
    $('#erroDialog').modal('show');
}

function showToast(type, title, msg) {
    var opts = {
        "closeButton": true,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "10000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    switch (type) {

        case 'info':
            toastr.info(msg, title, opts);
            break;
        case 'success':
            toastr.success(msg, title, opts);
            break;
        case 'error':
            toastr.error(msg, title, opts);
            break;
        case 'warning':
            toastr.warning(msg, title, opts);
            break;
        default :
            toastr.info(msg, title, opts);
            break;
    }

}

$(function () {
//lazy load
    if ($('img.lazy').size() > 0) {
        $('img.lazy').lazyload({
            effect: "fadeIn"
        });
    }

    if ($('.bg-lazy').size() > 0) {
        $('.bg-lazy').lazyload({
        });
    }

    //sidenav
    // Initialize collapse button
    if ($('.button-collapse').size() > 0) {
        $(".button-collapse").sideNav();
    }

    // Initialize collapsible (uncomment the line below if you use the dropdown variation)
    if ($('.collapsible').size() > 0) {
        $('.collapsible').collapsible();
    }

    $('[data-toggle="tooltip"]').tooltip({animation: true, delay: {show: 10, hide: 300}});


});

$(window).load(function () {
    $(".preloader .status").fadeOut();
    $(".preloader").delay(500).fadeOut("slow");
    new WOW().init();
});