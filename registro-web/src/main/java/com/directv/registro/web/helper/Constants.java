/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.helper;

/**
 *
 * @author dacopan
 */
public class Constants {

    public final static String SETUP_PAGE = "/faces/views/s/admin/setup.xhtml";
    public final static String DASHBOARD_URL = "/faces/views/dashboard/index.xhtml";
    public final static String STG_KEY_PORTAL = "STG_PORTAL";
    
    public static String DEFAULT_BACKGROUND="//res.cloudinary.com/dacopan/image/upload/v1457294704/March9_vmfbg8.jpg";
    public static String DEFAULT_PRIMARY_COLOR="#0277bd";
    public static String DEFAULT_SECONDARY_COLOR="#0277bd";
    public static String PUBLIC_USER_KEY="public";
    public static String DEFAULT_WIKI= "https://issuu.com/dacopan/docs/";
    public static String DEFAULT_SUPPORT="https://dacopancm.freshdesk.com/support/home";
    
    public static String DCM_SECRET="dacopanCMÍóÓúÚüÜñÑ";
    
    public static String JBPM_WEB_URL_KEY="registro_web_url";

    String text;

    /**
     * @param text
     */
    private Constants(final String text) {
        this.text = text;
    }

    /* (non-Javadoc)
     * @see java.lang.Enum#toString()
     */
    @Override
    public String toString() {
        return text;
    }

}
