/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.json;

import com.directv.registro.web.helper.Constants;
import java.io.Serializable;

/**
 *
 * @author dacopan
 */
public class SettingsPortal implements Serializable {

    private String name;
    private String descripcion;
    private String logo;
    private String google_client_id;
    private String google_client_secret;
    private String primary_color;
    private String secondary_color;
    private String background;
    private String weburl;

    private String wiki;
    private String support;

    public SettingsPortal() {
    }

    public SettingsPortal(String name, String descripcion, String logo, String google_client_id, String google_client_secret, String primary_color, String secondary_color, String background, String weburl, String wiki, String support) {
        this.name = name;
        this.descripcion = descripcion;
        this.logo = logo;
        this.google_client_id = google_client_id;
        this.google_client_secret = google_client_secret;
        this.primary_color = primary_color;
        this.secondary_color = secondary_color;
        this.background = background;
        this.weburl = weburl;
        this.wiki = wiki;
        this.support = support;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getGoogle_client_id() {
        return google_client_id;
    }

    public void setGoogle_client_id(String google_client_id) {
        this.google_client_id = google_client_id;
    }

    public String getGoogle_client_secret() {
        return google_client_secret;
    }

    public void setGoogle_client_secret(String google_client_secret) {
        this.google_client_secret = google_client_secret;
    }

    public String getPrimary_color() {
        if (primary_color == null) {
            return Constants.DEFAULT_PRIMARY_COLOR;
        }
        return primary_color;
    }

    public void setPrimary_color(String primary_color) {
        this.primary_color = primary_color;
    }

    public String getSecondary_color() {
        if (secondary_color == null) {
            return Constants.DEFAULT_SECONDARY_COLOR;
        }
        return secondary_color;
    }

    public void setSecondary_color(String secondary_color) {
        this.secondary_color = secondary_color;
    }

    public String getBackground() {
        if (background == null) {
            return Constants.DEFAULT_BACKGROUND;
        }
        return background;
    }

    public void setBackground(String background) {
        this.background = background;
    }

    public String getWeburl() {
        return weburl;
    }

    public void setWeburl(String weburl) {
        this.weburl = weburl;
    }

    public String getWiki() {
        if (wiki == null) {
            wiki = Constants.DEFAULT_WIKI;
        }
        return wiki;
    }

    public void setWiki(String wiki) {
        this.wiki = wiki;
    }

    public String getSupport() {
        if (support == null) {
            support = Constants.DEFAULT_SUPPORT;
        }
        return support;
    }

    public void setSupport(String support) {
        this.support = support;
    }

}
