/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.json;

/**
 *
 * @author dacopanCM
 */
public class Equipo {

    private String eqNombre;
    private String eqUid;

    public Equipo() {
    }

    public Equipo(String eqNombre, String eqUid) {
        this.eqNombre = eqNombre;
        this.eqUid = eqUid;
    }

    public String getEqNombre() {
        return eqNombre;
    }

    public void setEqNombre(String eqNombre) {
        this.eqNombre = eqNombre;
    }

    public String getEqUid() {
        return eqUid;
    }

    public void setEqUid(String eqUid) {
        this.eqUid = eqUid;
    }

}
