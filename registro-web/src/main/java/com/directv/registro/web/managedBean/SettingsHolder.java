/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.json.SettingsPortal;
import com.directv.registro.web.json.UserSettings;

import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author dacopan
 */
@Component("SettingsHolder")
@Scope(value = "session")
public class SettingsHolder implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @Value("${jom.portal.stage}")
    private String stage;

    @Value("${jom.jbpm.url}")
    private String bpm;

    private SettingsPortal portalSettings;
    private UserSettings userSettings;

    @PostConstruct
    public void postConstruct() {
        log.info("construct SettingsHolder:" + stage);
    }

    public SettingsPortal getPortalSettings() {
        if (portalSettings == null) {
            portalSettings = new SettingsPortal();

        }
        return portalSettings;
    }

    public void setPortalSettings(SettingsPortal portalSettings) {
        this.portalSettings = portalSettings;
    }

    public UserSettings getUserSettings() {
        if (userSettings == null) {
            userSettings = new UserSettings();
        }
        return userSettings;
    }

    public void setUserSettings(UserSettings userSettings) {
        this.userSettings = userSettings;
    }

    //stage
    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getBpm() {
        return bpm;
    }

    public void setBpm(String bpm) {
        this.bpm = bpm;
    }

    @PreDestroy
    public void preDestroy() {
        log.info("jom: preDestroy SettingsHolder");
    }
}
