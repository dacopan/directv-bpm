/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.dao;

import com.directv.registro.web.sp.model.ListaEsperaInstalacion;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dacopanCM
 */
@Repository
public class ListaInstalacionDAO extends GenericDAO<ListaEsperaInstalacion> {

    public List<ListaEsperaInstalacion> getAll() {
        return getSessionFactory().getCurrentSession().createQuery("from ListaEsperaInstalacion").list();
    }

    public ListaEsperaInstalacion getFirst() {
        List list = getSessionFactory().getCurrentSession().createQuery("select lst from ListaEsperaInstalacion lst inner join fetch lst.cliente as clt WHERE lst.lstInsFechaOut IS NULL order by lst.lstInsFechaIn desc").setMaxResults(1).list();
        return list.size() < 1 ? null : (ListaEsperaInstalacion) list.get(0);
    }
}
