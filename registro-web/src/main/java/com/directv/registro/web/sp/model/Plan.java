/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.model;

/**
 *
 * @author dacopanCM
 */
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "plan")
public class Plan implements java.io.Serializable {

    private Integer plnId;
    private String plnNombre;
    private Double plnPrecio;
    private Set<Cliente> clientes = new HashSet(0);

    public Plan() {
    }

    public Plan(String plnNombre, Double plnPrecio, Set clientes) {
        this.plnNombre = plnNombre;
        this.plnPrecio = plnPrecio;
        this.clientes = clientes;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "pln_id", unique = true, nullable = false)
    public Integer getPlnId() {
        return this.plnId;
    }

    public void setPlnId(Integer plnId) {
        this.plnId = plnId;
    }

    @Column(name = "pln_nombre", length = 45)
    public String getPlnNombre() {
        return this.plnNombre;
    }

    public void setPlnNombre(String plnNombre) {
        this.plnNombre = plnNombre;
    }

    @Column(name = "pln_precio", precision = 4, scale = 0)
    public Double getPlnPrecio() {
        return this.plnPrecio;
    }

    public void setPlnPrecio(Double plnPrecio) {
        this.plnPrecio = plnPrecio;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "plan")
    public Set<Cliente> getClientes() {
        return this.clientes;
    }

    public void setClientes(Set<Cliente> clientes) {
        this.clientes = clientes;
    }

}
