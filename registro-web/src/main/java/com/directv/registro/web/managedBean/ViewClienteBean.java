/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.model.Plan;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.JbpmService;
import com.directv.registro.web.sp.service.PlanService;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.web.util.UriUtils;

/**
 *
 * @author dacopanCM
 */
@ManagedBean(name = "viewClienteBean")
@ViewScoped
public class ViewClienteBean implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @ManagedProperty(value = "#{ClienteService}")
    private ClienteService clienteService;
    @ManagedProperty(value = "#{PlanService}")
    private PlanService planService;
    
    @ManagedProperty(value = "#{JbpmService}")
    private JbpmService jbpmService;

    private Cliente selectedCliente;
    private Integer selectedPlan;

    private List<Plan> listPlanes;

    private Integer cltId;
    private String hash;
    private String returnPage;

    public ViewClienteBean() {
    }

    @PostConstruct
    public void postConstruct() {
        //TODO handle when no or incomplete parameters
        Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        if (params.containsKey("cltId")) {
            cltId = Integer.parseInt(params.get("cltId"));
        } else {
            cltId = null;
            //error id y hash no coincide, estan modificando el url (hacking)
        }

        if (params.containsKey("h")) {
            hash = params.get("h");
        } else {
            //error id y hash no coincide, estan modificando el url (hacking)
        }

        if (params.containsKey("r")) {
            returnPage = params.get("r");
        } else {
            returnPage = "adminClientes";
        }

        try {
            String hash2 = UriUtils.decode(this.hash, "UTF-8");
            if (BCrypt.checkpw(cltId.toString(), hash2)) {
                selectedCliente = clienteService.getClienteById(cltId);
                if (selectedCliente.getPlan() != null) {
                    selectedPlan = selectedCliente.getPlan().getPlnId();
                }
                /////
            } else {
                //error id y hash no coincide, estan modificando el url (hacking)
            }
        } catch (UnsupportedEncodingException ex) {
            log.error("jom: error" + ex.getMessage(), ex);
        }

    }

    public void editClienteAction() {
        try {            

            Plan pln = new Plan();
            pln.setPlnId(selectedPlan);
            selectedCliente.setPlan(pln);
            
            clienteService.updateCliente(selectedCliente);
            jbpmService.raiseSignal(Long.parseLong(selectedCliente.getCltProcId()), "contrato_registrado_signal");

            selectedCliente = clienteService.getClienteById(cltId);

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Editar Cliente", "Editado con éxito"));
        } catch (CustomException ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Guardar", ex.getMessage()));
            clienteService.getClienteById(cltId);
            log.error("jom error editClienteAction: ", ex);
        } catch (Exception ex) {
            log.error("jom error editClienteAction: ", ex);
        }

    }

    public String retrieveContratoUrl() {
        return clienteService.getContratoUrl(selectedCliente.getCltCi(), FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath());
    }

    //get set    
    public Cliente getSelectedCliente() {
        return selectedCliente;
    }

    public void setSelectedCliente(Cliente selectedCliente) {
        this.selectedCliente = selectedCliente;
    }

    public Integer getSelectedPlan() {
        return selectedPlan;
    }

    public void setSelectedPlan(Integer selectedPlan) {
        this.selectedPlan = selectedPlan;
    }

    public List<Plan> getListPlanes() {
        if (listPlanes == null) {
            listPlanes = planService.getAllPlan();
        }
        return listPlanes;
    }

    public void setListPlanes(List<Plan> listPlanes) {
        this.listPlanes = listPlanes;
    }

    public Integer getCltId() {
        return cltId;
    }

    public void setCltId(Integer cltId) {
        this.cltId = cltId;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getReturnPage() {
        return returnPage;
    }

    public void setReturnPage(String returnPage) {
        this.returnPage = returnPage;
    }

    //services
    public ClienteService getClienteService() {
        return clienteService;
    }

    public void setClienteService(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    public PlanService getPlanService() {
        return planService;
    }

    public void setPlanService(PlanService planService) {
        this.planService = planService;
    }

    public JbpmService getJbpmService() {
        return jbpmService;
    }

    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }

}
