/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.JbpmService;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.event.FileUploadEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.util.UriUtils;

/**
 *
 * @author dacopanCM
 */
@ManagedBean(name = "adminClientesBean")
@ViewScoped
public class AdminClientesBean implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @ManagedProperty(value = "#{ClienteService}")
    private ClienteService clienteService;

    @ManagedProperty(value = "#{JbpmService}")
    private JbpmService jbpmService;

    private List<Cliente> listCliente;
    private Cliente selectedCliente;

    public AdminClientesBean() {
    }

    @PostConstruct
    public void postConstruct() {

    }

    public void resetAddCliente(ActionEvent actionEvent) {
        selectedCliente = new Cliente();

    }

    public void handleFileUpload(FileUploadEvent event) {

        try {
            clienteService.uploadContrato(selectedCliente, event.getFile());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded."));
            listCliente = null;
        } catch (CustomException ex) {
            log.error("error al guardar pdf", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Error al guardar pdf"));
        } catch (Exception ex) {
            log.error("error desconocido al guardar pdf", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Error al guardar pdf"));

        }

    }

    public String viewClienteRequestPageAction() {
        try {
            return "/views/s/viewCliente.xhtml?faces-redirect=true&cltId=" + selectedCliente.getCltId() + "&h=" + UriUtils.encode(new BCryptPasswordEncoder().encode(selectedCliente.getCltId().toString()), "UTF-8")
                    + "&r=adminClientes";
        } catch (UnsupportedEncodingException ex) {
            log.error("jom: " + ex, ex);
        }
        return null;
    }

    //get set
    public ClienteService getClienteService() {
        return clienteService;
    }

    public void setClienteService(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    //props
    public List<Cliente> getListCliente() {
        if (listCliente == null) {
            listCliente = clienteService.getAllCliente();
        }
        return listCliente;
    }

    public void setListCliente(List<Cliente> listCliente) {
        this.listCliente = listCliente;
    }

    public Cliente getSelectedCliente() {
        return selectedCliente;
    }

    public void setSelectedCliente(Cliente selectedCliente) {
        this.selectedCliente = selectedCliente;
    }

    public JbpmService getJbpmService() {
        return jbpmService;
    }

    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }
}
