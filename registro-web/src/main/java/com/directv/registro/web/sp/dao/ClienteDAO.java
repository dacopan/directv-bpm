/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.dao;

import com.directv.registro.web.sp.model.Cliente;
import java.util.List;
import org.springframework.stereotype.Repository;

/**
 *
 * @author dacopanCM
 */
@Repository
public class ClienteDAO extends GenericDAO<Cliente> {

    public List<Cliente> getAll() {
        return getSessionFactory().getCurrentSession().createQuery("from Cliente").list();
    }

    public Cliente getByCi(String cltCi) {
        List list = getSessionFactory().getCurrentSession().createQuery("from Cliente where cltCi=:cltCi").setParameter("cltCi", cltCi).list();
        return list.size() < 1 ? null : (Cliente) list.get(0);
    }

    public Cliente getById(Integer cltId) {
        List list = getSessionFactory().getCurrentSession().createQuery("from Cliente where cltId=:cltId").setParameter("cltId", cltId).list();
        return list.size() < 1 ? null : (Cliente) list.get(0);
    }
}
