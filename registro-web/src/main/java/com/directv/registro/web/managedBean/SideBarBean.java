/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.helper.HelperUtil;
import com.directv.registro.web.json.UserGoogle;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author dacopan
 */
@ManagedBean(name = "SideBarBean")
@SessionScoped
public class SideBarBean implements Serializable {

    private final Log log = LogFactory.getLog(getClass());
    private UserGoogle user;

    @PostConstruct
    public void postConstruct() {
        getUser();
    }

    public boolean isAuthenticated() {
        return getUser() != null;
    }    

    public UserGoogle getUser() {
        if (user == null) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            Object principal = (auth == null) ? null : auth.getPrincipal();
            if (principal != null && principal instanceof UserGoogle) {
                user = (UserGoogle) principal;
            }
        }
        return user;
    }

    public void setUser(UserGoogle user) {
        this.user = user;
    }

    public String getGravatar() {
        String picture = "http://www.gravatar.com/avatar/00000000000000000000000000000000?d=identicon";        
        if (getUser() != null) {
            if (user.getPictureUrl() == null) {
                String hash = HelperUtil.md5Hex(user.getEmail());
                if (hash != null) {
                    picture = "http://www.gravatar.com/avatar/" + hash + "?d=identicon";
                }

            } else {
                picture = user.getPictureUrl();
            }
        }

        return picture;

    }
    
    @PreDestroy
    public void preDestroy() {
        log.info("jom: preDestroy SideBarBean");
    }

}
