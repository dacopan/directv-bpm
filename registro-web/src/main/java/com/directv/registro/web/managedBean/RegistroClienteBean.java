/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.JbpmService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dacopanCM
 */
@ManagedBean(name = "registroClienteBean")
@ViewScoped
public class RegistroClienteBean implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @ManagedProperty(value = "#{ClienteService}")
    private ClienteService clienteService;

    @ManagedProperty(value = "#{JbpmService}")
    private JbpmService jbpmService;

    private Cliente selectedCliente;
    private Boolean needAsistencia;

    public RegistroClienteBean() {
    }

    @PostConstruct
    public void postConstruct() {
        resetAddCliente(null);
    }

    public void resetAddCliente(ActionEvent actionEvent) {
        selectedCliente = new Cliente();
    }

    /**
     * anade cliente nuevo
     */
    public void addClienteAction() {
        try {

            clienteService.addCliente(selectedCliente, needAsistencia);

            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Añadir Cliente", "Añadido con éxito"));
        } catch (CustomException ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Guardar", ex.getMessage()));
            log.error("jom error addClienteAction: ", ex);
        } catch (Exception ex) {
            log.error("jom error addClienteAction: ", ex);
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error Guardar", ex.getMessage()));
        }

    }

    public ClienteService getClienteService() {
        return clienteService;
    }

    public void setClienteService(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    public JbpmService getJbpmService() {
        return jbpmService;
    }

    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }

    public Cliente getSelectedCliente() {
        return selectedCliente;
    }

    public void setSelectedCliente(Cliente selectedCliente) {
        this.selectedCliente = selectedCliente;
    }

    public Boolean getNeedAsistencia() {
        return needAsistencia;
    }

    public void setNeedAsistencia(Boolean needAsistencia) {
        this.needAsistencia = needAsistencia;
    }

}
