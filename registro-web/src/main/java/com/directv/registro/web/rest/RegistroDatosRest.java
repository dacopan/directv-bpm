/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.rest;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.helper.HelperUtil;
import com.directv.registro.web.json.RestResponse;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.EmailService;
import com.directv.registro.web.sp.service.JbpmService;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dacopanCM
 */
@RestController
@RequestMapping(value = "/registro", produces = MediaType.APPLICATION_JSON_VALUE)
public class RegistroDatosRest {

    private final Log log = LogFactory.getLog(getClass());
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private JbpmService jbpmService;
    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/greeting")
    public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return counter.incrementAndGet() + String.format("Hello, %s!", name);
    }

    @RequestMapping(value = "/notificarCancelarRegistro/{cltId}")
    public RestResponse notificarCancelarRegistro(@PathVariable Integer cltId, @RequestBody RestResponse reason) {
        try {
            Cliente clt = clienteService.getClienteById(cltId);

            clienteService.updateEstado(clt, "cancelado");
            //TODO email
            emailService.notificarCancelarRegistro(clt, reason.getData().toString());
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarCancelarRegistro/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/validardatos/{cltId}")
    public RestResponse validardatos(@PathVariable String cltId) {
        try { //return counter.incrementAndGet() + String.format(template, petId);
            String result = "true";
            boolean _res;
            //final JsonNodeFactory factory = JsonNodeFactory.instance;

            Cliente clt = clienteService.getClienteById(Integer.parseInt(cltId));
            _res = HelperUtil.isValidCI(clt.getCltCi());

            return new RestResponse(200, _res);
        } catch (NumberFormatException ex) {
            log.error("error en rest /validardatos/{cltId}", ex);
            return new RestResponse(500, false);
        }
    }

    @RequestMapping(value = "/notificarDatosInvalidos/{cltId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public RestResponse notificarDatosInvalidos(@PathVariable Integer cltId, @RequestBody RestResponse reason) {
        try {
            //TODO email
            Cliente clt = clienteService.getClienteById(cltId);
            emailService.notificarDatosInvalidos(clt, reason.getData().toString());
            return new RestResponse(200, true);
        } catch (Exception ex) {
            log.error("error en rest notificarDatosInvalidos", ex);
            return new RestResponse(500, false);
        }
    }

}
