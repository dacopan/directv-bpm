/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.service;

import com.directv.registro.web.sp.model.Cliente;
import java.io.Serializable;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.kie.api.cdi.KSession;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.api.runtime.process.ProcessInstance;
import org.kie.api.runtime.process.WorkflowProcessInstance;
import org.kie.api.task.TaskService;
import org.kie.api.task.model.TaskSummary;
import org.kie.remote.client.api.RemoteRestRuntimeEngineBuilder;
import org.kie.remote.client.api.RemoteRuntimeEngineFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 *
 * @author dacopanCM
 */
@Service("JbpmService")
public class JbpmService implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @Value("${jom.jbpm.url}")
    private String url;

    @Value("${jom.jbpm.deploymentId}")
    private String deploymentId;

    @Value("${jom.jbpm.user}")
    private String user;

    @Value("${jom.jbpm.password}")
    private String password;

    public long startProcess(Cliente clt, Boolean clt_asistencia, Boolean clt_is_prepago, String urlxx, String contrato_url, String clt_edit_url) {
        KieSession ksession = getKsession();

        Map<String, Object> params = new HashMap<>();
        params.put("clt", clt);
        params.put("clt_is_prepago", clt_is_prepago);
        params.put("clt_asistencia", clt_asistencia);
        params.put("registro_web_url", urlxx);
        params.put("clt_contrato_url", contrato_url);
        params.put("clt_edit_url", clt_edit_url);
        params.put("clt_id", clt.getCltId());

        ProcessInstance processInstance
                = ksession.startProcess("registro", params);
        long procId = processInstance.getId();
        log.info("dcm: process instance create with ID: " + procId);
        return procId;
    }

    private KieSession getKsession() {
        try {
            RuntimeEngine engine = getRuntimeEngine();

            // Create KieSession and TaskService instances and use them
            KieSession ksession = engine.getKieSession();
            return ksession;
        } catch (Exception ex) {
            log.error("error en crear ksession", ex);
            return null;
        }
    }

    private RuntimeEngine getRuntimeEngine() {
        try {
            RuntimeEngine engine = RemoteRuntimeEngineFactory.newRestBuilder()
                    .addUrl(new URL(url))
                    .addTimeout(5)
                    .addDeploymentId(deploymentId)
                    .addUserName(user)
                    .addPassword(password)
                    // if you're sending custom class parameters, make sure that
                    // the remote client instance knows about them!
                    .addExtraJaxbClasses(Cliente.class)
                    .build();
            return engine;
        } catch (Exception ex) {
            log.error("error en crear RuntimeEngine", ex);
            return null;
        }
    }

    public void startProcessAndHandleTaskViaRestRemoteJavaAPI(URL serverRestUrl, String deploymentId, String user, String password) {
        // the serverRestUrl should contain a URL similar to "http://localhost:8080/jbpm-console/"

        // Setup the factory class with the necessarry information to communicate with the REST services
        RuntimeEngine engine = RemoteRuntimeEngineFactory.newRestBuilder()
                .addUrl(serverRestUrl)
                .addTimeout(5)
                .addDeploymentId(deploymentId)
                .addUserName(user)
                .addPassword(password)
                // if you're sending custom class parameters, make sure that
                // the remote client instance knows about them!
                .addExtraJaxbClasses(Cliente.class)
                .build();

        // Create KieSession and TaskService instances and use them
        KieSession ksession = engine.getKieSession();
        TaskService taskService = engine.getTaskService();

        // Each operation on a KieSession, TaskService or AuditLogService (client) instance
        // sends a request for the operation to the server side and waits for the response
        // If something goes wrong on the server side, the client will throw an exception.
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("paramName", new Cliente());
        ProcessInstance processInstance
                = ksession.startProcess("com.burns.reactor.maintenance.cycle", params);
        long procId = processInstance.getId();

        String taskUserId = user;
        taskService = engine.getTaskService();
        List<TaskSummary> tasks = taskService.getTasksAssignedAsPotentialOwner(user, "en-UK");

        long taskId = -1;
        for (TaskSummary task : tasks) {
            if (task.getProcessInstanceId() == procId) {
                taskId = task.getId();
            }
        }

        if (taskId == -1) {
            throw new IllegalStateException("Unable to find task for " + user
                    + " in process instance " + procId);
        }

        taskService.start(taskId, taskUserId);

        // resultData can also just be null
        Map<String, Object> resultData = new HashMap<>();
        taskService.complete(taskId, taskUserId, resultData);
    }

    public void raiseSignal(long procID, String signal) {
        getKsession().signalEvent(signal, null, procID);
    }

    /**
     * //TODO no funciona
     *
     * @param procID
     * @param name
     * @param value
     */
    public void setVariable(long procID, String name, Object value) {
        ((WorkflowProcessInstance) getKsession().getProcessInstance(procID)).setVariable(name, value);
    }

    public void clearJbpmLog() {
        getRuntimeEngine().getAuditService().clear();
         getRuntimeEngine().getTaskService().exit(0, url);
    }
}
