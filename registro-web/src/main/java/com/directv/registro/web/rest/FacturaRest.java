/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.rest;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.json.RestResponse;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.EmailService;
import com.directv.registro.web.sp.service.JbpmService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dacopanCM
 */
@RestController
@RequestMapping(value = "/factura", produces = MediaType.APPLICATION_JSON_VALUE)
public class FacturaRest {

    private final Log log = LogFactory.getLog(getClass());
    @Autowired
    private JbpmService jbpmService;
    @Autowired
    private ClienteService clienteService;

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/enviarFactura/{cltId}", method = RequestMethod.POST)
    public RestResponse enviarFactura(@PathVariable Integer cltId) {
        try {
            //TODO email
            Cliente clt = clienteService.getClienteById(cltId);
            String valorFactura = "125.99";
            emailService.enviarFactura(clt, valorFactura);
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /validardatos/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/notificarPagonNoValido/{cltId}", method = RequestMethod.POST)
    public RestResponse notificarPagonNoValido(@PathVariable Integer cltId, @RequestBody RestResponse reason) {
        try {
            //TODO email
            Cliente clt = clienteService.getClienteById(cltId);
            emailService.notificarPagonNoValido(clt, reason.getData().toString());
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarPagonNoValido/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/aprobarSuscripcion/{cltId}", method = RequestMethod.POST)
    public RestResponse aprobarSuscripcion(@PathVariable Integer cltId) {
        try {

            Cliente clt = clienteService.getClienteById(cltId);
            clienteService.updateEstado(clt, "aprobado");
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /aprobarSuscripcion/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/notificarSuscripcion/{cltId}", method = RequestMethod.POST)
    public RestResponse notificarSuscripcion(@PathVariable Integer cltId) {
        try {
            //TODO email
            Cliente clt = clienteService.getClienteById(cltId);
            emailService.notificarSuscripcion(clt);
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarSuscripcion/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }
}
