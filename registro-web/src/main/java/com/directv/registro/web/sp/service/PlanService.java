/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.service;

import com.directv.registro.web.sp.dao.PlanDAO;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.model.Plan;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author dacopanCM
 */
@Service("PlanService")
@Transactional(readOnly = true)
public class PlanService implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @Autowired
    private PlanDAO planDAO;

    public List<Plan> getAllPlan() {
        return planDAO.getAll();

    }

    public Plan getPlanById(Integer plnId) {
        return planDAO.getById(plnId);
    }

}
