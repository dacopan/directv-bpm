/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.model;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.helper.HelperUtil;
import com.directv.registro.web.json.Equipo;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author dacopanCM
 */
@Entity
@Table(name = "cliente")
public class Cliente implements java.io.Serializable {

    private Integer cltId;
    private Plan plan;
    private String cltNombre;
    private String cltApellido;
    private String cltCi;
    private String cltTelefono;
    private String cltEmail;
    private String cltDireccion;
    private String cltContrato;
    private Boolean cltInstalado;
    private String cltProcId;
    private String cltEstado;
    private String cltEquipos;
    private String cltEncuesta;
    private Boolean cltIsPrepago;
    private Set<ListaEsperaInstalacion> listaEsperaInstalacions = new HashSet(0);

    private ArrayList equipos;
    private HashMap<String, String> encuesta;

    public Cliente() {
    }

    public Cliente(Plan plan) {
        this.plan = plan;
    }

    public Cliente(Integer cltId, Plan plan, String cltNombre, String cltApellido, String cltCi, String cltTelefono, String cltEmail, String cltDireccion, String cltContrato, Boolean cltInstalado, String cltProcId, String cltEstado, String cltEquipos, String cltEncuesta, Boolean cltIsPrepago, ArrayList equipos) {
        this.cltId = cltId;
        this.plan = plan;
        this.cltNombre = cltNombre;
        this.cltApellido = cltApellido;
        this.cltCi = cltCi;
        this.cltTelefono = cltTelefono;
        this.cltEmail = cltEmail;
        this.cltDireccion = cltDireccion;
        this.cltContrato = cltContrato;
        this.cltInstalado = cltInstalado;
        this.cltProcId = cltProcId;
        this.cltEstado = cltEstado;
        this.cltEquipos = cltEquipos;
        this.cltEncuesta = cltEncuesta;
        this.cltIsPrepago = cltIsPrepago;
        this.equipos = equipos;
    }

    @Id
    @GeneratedValue(strategy = IDENTITY)

    @Column(name = "clt_id", unique = true, nullable = false)
    public Integer getCltId() {
        return this.cltId;
    }

    public void setCltId(Integer cltId) {
        this.cltId = cltId;
    }

    @XmlTransient
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pln_id", nullable = false)
    public Plan getPlan() {
        return this.plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    @Column(name = "clt_nombre", length = 45)
    public String getCltNombre() {
        return this.cltNombre;
    }

    public void setCltNombre(String cltNombre) {
        this.cltNombre = cltNombre;
    }

    @Column(name = "clt_apellido", length = 45)
    public String getCltApellido() {
        return this.cltApellido;
    }

    public void setCltApellido(String cltApellido) {
        this.cltApellido = cltApellido;
    }

    @Column(name = "clt_ci", length = 10)
    public String getCltCi() {
        return this.cltCi;
    }

    public void setCltCi(String cltCi) {
        this.cltCi = cltCi;
    }

    @Column(name = "clt_telefono", length = 10)
    public String getCltTelefono() {
        return this.cltTelefono;
    }

    public void setCltTelefono(String cltTelefono) {
        this.cltTelefono = cltTelefono;
    }

    @Column(name = "clt_email", length = 45)
    public String getCltEmail() {
        return this.cltEmail;
    }

    public void setCltEmail(String cltEmail) {
        this.cltEmail = cltEmail;
    }

    @Column(name = "clt_direccion", length = 80)
    public String getCltDireccion() {
        return this.cltDireccion;
    }

    public void setCltDireccion(String cltDireccion) {
        this.cltDireccion = cltDireccion;
    }

    @Column(name = "clt_contrato", length = 80)
    public String getCltContrato() {
        return this.cltContrato;
    }

    public void setCltContrato(String cltContrato) {
        this.cltContrato = cltContrato;
    }

    @Column(name = "clt_instalado")
    public Boolean getCltInstalado() {
        return this.cltInstalado;
    }

    public void setCltInstalado(Boolean cltInstalado) {
        this.cltInstalado = cltInstalado;
    }

    @Column(name = "clt_proc_id", nullable = false, length = 80)
    public String getCltProcId() {
        return cltProcId;
    }

    public void setCltProcId(String cltProcId) {
        this.cltProcId = cltProcId;
    }

    @Column(name = "clt_estado", nullable = false, length = 45)
    public String getCltEstado() {
        return cltEstado;
    }

    public void setCltEstado(String cltEstado) {
        this.cltEstado = cltEstado;
    }

    @Column(name = "clt_is_prepago")
    public Boolean getCltIsPrepago() {
        return cltIsPrepago;
    }

    public void setCltIsPrepago(Boolean cltIsPrepago) {
        this.cltIsPrepago = cltIsPrepago;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "cliente",cascade = CascadeType.ALL)
    @XmlTransient
    public Set<ListaEsperaInstalacion> getListaEsperaInstalacions() {
        return this.listaEsperaInstalacions;
    }

    public void setListaEsperaInstalacions(Set<ListaEsperaInstalacion> listaEsperaInstalacions) {
        this.listaEsperaInstalacions = listaEsperaInstalacions;
    }

    @Column(name = "clt_equipos", nullable = true, length = 65535)
    public String getCltEquipos() {
        return cltEquipos;
    }

    public void setCltEquipos(String cltEquipos) {
        this.cltEquipos = cltEquipos;
    }

    @Column(name = "clt_encuesta", nullable = true, length = 65535)
    public String getCltEncuesta() {
        return cltEncuesta;
    }

    public void setCltEncuesta(String cltEncuesta) {
        this.cltEncuesta = cltEncuesta;
    }

    @Transient
    public ArrayList<Equipo> getEquipos() {
        if (equipos == null) {
            if (cltEquipos != null && !cltEquipos.trim().isEmpty()) {
                try {
                    equipos = HelperUtil.fromJSON(cltEquipos, new TypeReference<ArrayList<Equipo>>() {
                    });

                } catch (CustomException ex) {

                }
            }
        }
        return equipos;
    }

    @Transient
    public HashMap<String, String> getEncuesta() {
        if (encuesta == null) {
            if (cltEncuesta != null && !cltEncuesta.trim().isEmpty()) {
                try {
                    encuesta = HelperUtil.fromJSON(cltEncuesta, new TypeReference<HashMap<String, String>>() {
                    });

                } catch (CustomException ex) {

                }
            }
        }
        return encuesta;
    }

    //custom
    @Transient
    public String getFullName() {
        return new StringBuilder().append(cltApellido).append(" ").append(cltNombre).toString();
    }

}
