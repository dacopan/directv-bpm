/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.helper;

import com.directv.registro.web.exceptions.CustomException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Date;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dacopan
 */
public class HelperUtil {

    private static final Log LOG = LogFactory.getLog(HelperUtil.class);

    public static <T> T fromJSON(final String jsonPacket, Class<T> cl) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        T data = mapper.readValue(jsonPacket, cl);
        return data;
    }

    public static <T> T fromJSON(final String jsonPacket, final TypeReference<T> type) throws CustomException {
        T data = null;

        try {
            data = new ObjectMapper().readValue(jsonPacket, type);
        } catch (Exception e) {
            LOG.error("dcm:error fromjson: " + jsonPacket, e);
            throw new CustomException("error deserializando json");
        }
        return data;
    }

    public static String toJSON(Object objectToJson) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        return mapper.writeValueAsString(objectToJson);

    }

    public static String hex(byte[] array) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; ++i) {
            sb.append(Integer.toHexString((array[i]
                    & 0xFF) | 0x100).substring(1, 3));
        }
        return sb.toString();
    }

    public static String md5Hex(String message) {
        try {
            MessageDigest md
                    = MessageDigest.getInstance("MD5");
            return hex(md.digest(message.getBytes("CP1252")));
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            LOG.error("ssod error: no se pudo generar md5 para gravatar", e);
        }
        return null;
    }

    public static String getMemoryUse() {
        int mb = 1048576;//1024*1024

        //Getting the runtime reference from system
        Runtime runtime = Runtime.getRuntime();
        long used = (runtime.totalMemory() - runtime.freeMemory()) / mb;
        long free = runtime.freeMemory() / mb;
        long total = runtime.totalMemory() / mb;
        long max = runtime.maxMemory() / mb;
        double percent = (used / ((double) total)) * 100.0;

        String tipo = "progress-bar-";
        if (percent <= 50) {
            tipo += "success";
        } else if (percent <= 75) {
            tipo += "warning";
        } else {
            tipo += "danger";
        }

        String bar = String.format("\n<div class=\"progress\"><div class='progress-bar active %s' style='width:%s%%'></div></div>\n", tipo, percent);

        StringBuilder sb = new StringBuilder();

        sb.append("##### Heap utilization statistics [MB] #####\n")
                .append(new Date())
                .append("\nUsed Memory:")
                .append(used).append(String.format(" (%.2f%%)", percent))
                .append("\nFree Memory:")
                .append(free)
                .append("\nTotal Memory:")
                .append(total)
                .append("\nMax Memory:")
                .append(max);

        LOG.info(sb.toString());
        return sb.append(bar).toString().replace("\n", "<br/>");
    }

    public static String getMemoryUse(boolean b) {
        int mb = 1048576;//1024*1024
        Runtime runtime = Runtime.getRuntime();
        StringBuilder sb = new StringBuilder();
        sb.append("Used Memory:")
                .append((runtime.totalMemory() - runtime.freeMemory()) / mb);
        LOG.info(sb.toString());
        return sb.toString().replace("\n", "<br/>");
    }

    public static String cifrarClave(String clave) {
        String secretKey = Constants.DCM_SECRET; //llave para encriptar datos
        String base64EncryptedString = "xx";

        try {

            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

            SecretKey key = new SecretKeySpec(keyBytes, "DESede");
            Cipher cipher = Cipher.getInstance("DESede");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            byte[] plainTextBytes = clave.getBytes("utf-8");
            byte[] buf = cipher.doFinal(plainTextBytes);
            byte[] base64Bytes = Base64.encodeBase64(buf);
            base64EncryptedString = new String(base64Bytes);

        } catch (Exception ex) {
        }
        return base64EncryptedString;
    }

    public static String decifrarClave(String textoEncriptado) {
        String secretKey = Constants.DCM_SECRET; //llave para desenciptar datos
        String base64EncryptedString = "xx";

        try {
            byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
            byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            SecretKey key = new SecretKeySpec(keyBytes, "DESede");

            Cipher decipher = Cipher.getInstance("DESede");
            decipher.init(Cipher.DECRYPT_MODE, key);

            byte[] plainText = decipher.doFinal(message);

            base64EncryptedString = new String(plainText, "UTF-8");

        } catch (Exception ex) {
        }
        return base64EncryptedString;
    }

    public static boolean isValidCI(String ci) {
        //*
        char[] ced = ci.toCharArray();
        int isNumeric;
        int total = 0;
        int tamanoLongitudCedula = 10;
        int[] coeficientes = new int[]{2, 1, 2, 1, 2, 1, 2, 1, 2};
        int numeroProvincias = 24;
        int tercerDigito = 6;

        if (ced.length == tamanoLongitudCedula) {
            int provincia = Integer.parseInt(ced[0] + "" + ced[1]);
            int digitoTres = Integer.parseInt(ced[2] + "");
            if ((provincia > 0 && provincia <= numeroProvincias) && digitoTres < tercerDigito) {
                int digitoVerificadorRecibido = Integer.parseInt(ced[9] + "");
                for (int k = 0; k < coeficientes.length; k++) {
                    int valor = Integer.parseInt(coeficientes[k] + "")
                            * Integer.parseInt(ced[k] + "");
                    total = valor >= 10 ? total + (valor - 9) : total + valor;

                }
                int digitoVerificadorObtenido = total >= 10 ? (total % 10) != 0
                        ? 10 - (total % 10) : (total % 10) : total;
                return digitoVerificadorObtenido == digitoVerificadorRecibido;

            }
            return false;

        } else {
            return false;
        }
        //*
    }

}
