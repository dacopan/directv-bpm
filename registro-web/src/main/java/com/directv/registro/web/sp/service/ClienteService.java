/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.service;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.sp.dao.ClienteDAO;
import com.directv.registro.web.sp.dao.ListaInstalacionDAO;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.model.ListaEsperaInstalacion;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.util.UriUtils;

/**
 *
 * @author dacopanCM
 */
@Service("ClienteService")
@Transactional(readOnly = true)
public class ClienteService implements Serializable {
    
    private final Log log = LogFactory.getLog(getClass());
    
    @Autowired
    private ClienteDAO clienteDAO;
    
    @Autowired
    private ListaInstalacionDAO listaInstalacionDAO;
    
    @Value("${jom.portal.uploaddir}")
    private String uploadDir;
    
    @Value("${jom.portal.url}")
    private String urlx;
    
    @Autowired
    private JbpmService jbpmService;
    
    @Transactional(readOnly = false)
    public void addCliente(Cliente c, Boolean needAsistencia) throws CustomException {
        Cliente old = clienteDAO.getByCi(c.getCltCi());
        if (old != null) {
            throw new CustomException("Cliente con ese CI ya existe");
        }
        //save client
        clienteDAO.save(c);
        c = clienteDAO.getByCi(c.getCltCi());

        //init process
        String contextx = FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath();
        long procid = jbpmService.startProcess(c, needAsistencia, c.getCltIsPrepago(),
                urlx + contextx,
                getContratoUrl(c.getCltCi(), contextx),
                getEditClientUrl(c.getCltId(), contextx)
        );

        //save proc id in clt        
        c.setCltProcId(procid + "");
        c.setCltEstado("en proceso");
        clienteDAO.update(c);
    }
    
    @Transactional(readOnly = false)
    public void updateCliente(Cliente c) throws CustomException {
        Cliente old = clienteDAO.getByCi(c.getCltCi());
        if (old != null && old.getCltId().intValue() != c.getCltId()) {
            clienteDAO.evict(old);
            clienteDAO.evict(c);
            throw new CustomException("Cliente con ese CI ya existe");
        }
        if (old != null) {
            clienteDAO.evict(old);
        }
        
        clienteDAO.update(c);
    }
    
    @Transactional(readOnly = false)
    public void deleteCliente(Cliente c) throws CustomException {
        clienteDAO.delete(c);
    }
    
    public List<Cliente> getAllCliente() {
        return clienteDAO.getAll();
        
    }
    
    public Cliente getClienteByCi(String mrcNombre) {
        return clienteDAO.getByCi(mrcNombre);
    }
    
    public Cliente getClienteById(Integer cltId) {
        return clienteDAO.getById(cltId);
    }
    
    @Transactional(readOnly = false)
    public Map<String, String> uploadContrato(Cliente clt, UploadedFile file) throws CustomException {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        HashMap<String, String> res = null;
        try {
            String signal = "contrato_firmado_signal";
            File f = new File(uploadDir + "/contrato_" + clt.getCltCi() + ".pdf");
            log.info("dcm: filepath:" + uploadDir + "/contrato_" + clt.getCltCi() + ".pdf");
            if (f.exists()) {//contrato existe entonces es un reemplazo
                log.info("file:" + f.getCanonicalPath() + " exist: " + f.getName());
                f.delete();
                f.createNewFile();
            } else {
                f.createNewFile();
            }
            if (clt.getCltContrato() != null && !clt.getCltContrato().trim().isEmpty()) {
                signal = "reingreso_contrato_signal";
            }

            //write file
            // read this file into InputStream
            inputStream = file.getInputstream();

            // write the inputStream to a FileOutputStream
            outputStream = new FileOutputStream(f);
            
            int read;
            byte[] bytes = new byte[1024];
            
            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            
            inputStream.close();
            outputStream.close();

            //end write
            clt.setCltContrato(f.getName());
            updateCliente(clt);
            
            res = new HashMap();
            res.put("contrato_url", getContratoUrl(clt.getCltCi(), FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()));
            res.put("signal", signal);

            //raise signal
            //jbpmService.setVariable(Long.parseLong(clt.getCltProcId()), "contrato_url", getContratoUrl(clt.getCltCi(), FacesContext.getCurrentInstance().getExternalContext().getRequestContextPath()));
            jbpmService.raiseSignal(Long.parseLong(clt.getCltProcId()), signal);
        } catch (IOException | CustomException ex) {
            log.error("error en uploadContrato:", ex);
            throw new CustomException("error en uploadContrato");
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException ex) {
                    log.error("error on close inputStream:", ex);
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException ex) {
                    log.error("error on close outputStream:", ex);
                }
                
            }
            
        }
        return res;
    }
    
    public String getContratoPath(String clt_ci) {
        return new File(uploadDir + "/contrato_" + clt_ci + ".pdf").getAbsolutePath();
        
    }
    
    public String getContratoPathFromFilename(String filename) {
        return new File(uploadDir + filename).getAbsolutePath();
        
    }
    
    public String getContratoUrl(String clt_ci, final String context) {
        return urlx + context + "/uploads/contratos/contrato_" + clt_ci + ".pdf";
    }
    
    public String getEditClientUrl(Integer clt_id, final String context) throws CustomException {
        try {
            return urlx + context + "/faces/views/s/viewCliente.xhtml?cltId=" + clt_id + "&h=" + UriUtils.encode(new BCryptPasswordEncoder().encode(clt_id.toString()), "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            throw new CustomException("error al obtener url de edicion de cliente");
        }
    }
    
    @Transactional(readOnly = false)
    public void updateEstado(Cliente clt, String estado) throws CustomException {
        clt.setCltEstado(estado);
        updateCliente(clt);
    }
    
    @Transactional(readOnly = false)
    public void removeListaEspera(ListaEsperaInstalacion lst) throws CustomException {
        listaInstalacionDAO.delete(lst);
    }
}
