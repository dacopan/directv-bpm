/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.rest;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.helper.HelperUtil;
import com.directv.registro.web.json.Equipo;
import com.directv.registro.web.json.RestResponse;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.model.ListaEsperaInstalacion;
import com.directv.registro.web.sp.service.ClienteService;
import com.directv.registro.web.sp.service.EmailService;
import com.directv.registro.web.sp.service.JbpmService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author dacopanCM
 */
@RestController
@RequestMapping(value = "/instalacion", produces = MediaType.APPLICATION_JSON_VALUE)
public class InstalacionRest {

    private final Log log = LogFactory.getLog(getClass());
    @Autowired
    private JbpmService jbpmService;
    @Autowired
    private ClienteService clienteService;
    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/agregarAListaEspera/{cltId}", method = RequestMethod.POST)
    public RestResponse agregarAListaEspera(@PathVariable Integer cltId, @RequestBody RestResponse fecha_instalacion) {
        try {
            //change estado
            Cliente clt = clienteService.getClienteById(cltId);
            clienteService.updateEstado(clt, "en espera instalacion");
            //add to queue instalacion
            Set<ListaEsperaInstalacion> lista = new HashSet<>();
            lista.add(new ListaEsperaInstalacion(clt, new Date()));
            clt.setListaEsperaInstalacions(lista);

            clienteService.updateCliente(clt);

            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarFechaInstalacion/{cltId}", ex);
            return new RestResponse(500, false);
        }
    }

    @RequestMapping(value = "/notificarFechaInstalacion/{cltId}", method = RequestMethod.POST)
    public RestResponse notificarFechaInstalacion(@PathVariable Integer cltId, @RequestBody RestResponse fecha_instalacion) {
        try {
            //change estado
            Cliente clt = clienteService.getClienteById(cltId);
            clienteService.updateEstado(clt, "pendiente instalacion");
            //save fecha
            ListaEsperaInstalacion lst = (ListaEsperaInstalacion) clt.getListaEsperaInstalacions().toArray()[0];

            /*lst.setLstInsFechaOut(Date.from(
                    LocalDateTime.parse(fecha_instalacion.getData().toString())
                    .atZone(ZoneId.systemDefault()).toInstant()
            ));*/
            lst.setLstInsFechaOut(new Date(Long.parseLong(fecha_instalacion.getData().toString())));

            clienteService.updateCliente(clt);
            //TODO email
            emailService.notificarFechaInstalacion(clt, lst.getLstInsFechaOut());
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarFechaInstalacion/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/notificarFalloInstalacion/{cltId}", method = RequestMethod.POST)
    public RestResponse notificarFalloInstalacion(@PathVariable Integer cltId, @RequestBody RestResponse reason) {
        try {
            //TODO email
            Cliente clt = clienteService.getClienteById(cltId);
            emailService.notificarFalloInstalacion(clt, reason.getData().toString());
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /notificarFalloInstalacion/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/eliminarDeListaInstalacion/{cltId}", method = RequestMethod.POST)
    public RestResponse eliminarDeListaInstalacion(@PathVariable Integer cltId) {
        try {
            //change estado
            Cliente clt = clienteService.getClienteById(cltId);
            clienteService.updateEstado(clt, "instalado");
            //delete from cola instalacion
            ListaEsperaInstalacion lst = (ListaEsperaInstalacion) clt.getListaEsperaInstalacions().toArray()[0];

            clt.getListaEsperaInstalacions().clear();

            clienteService.updateCliente(clt);
            clienteService.removeListaEspera(lst);
            return new RestResponse(200, true);
        } catch (NumberFormatException | CustomException ex) {
            log.error("error en rest /eliminarDeListaInstalacion/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/activarEquipos/{cltId}", method = RequestMethod.POST)
    public RestResponse activarEquipos(@PathVariable Integer cltId, @RequestBody RestResponse equipos_json) {
        try {
            Cliente clt = clienteService.getClienteById(cltId);

            //save equipos
            HashMap<String, String> equiposMap = (HashMap<String, String>) equipos_json.getData();

            ArrayList<Equipo> equipos = new ArrayList<>();

            equiposMap.forEach((k, v) -> {
                equipos.add(new Equipo(k, v));
            });

            clt.setCltEquipos(HelperUtil.toJSON(equipos));

            clienteService.updateCliente(clt);

            return new RestResponse(200, true);
        } catch (NumberFormatException | JsonProcessingException | CustomException ex) {
            log.error("error en rest /activarEquipos/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    /**
     * not implemented, uplicated activarEquipos
     *
     * @param cltId
     * @param equipos_json
     * @return
     */
    @RequestMapping(value = "/activarEquiposPrepago/{cltId}", method = RequestMethod.POST)
    public RestResponse activarEquiposPrepago(@PathVariable Integer cltId, @RequestBody RestResponse equipos_json) {
        try {
            //TODO save equipos
            return new RestResponse(200, true);
        } catch (NumberFormatException ex) {
            log.error("error en rest /activarEquiposPrepago/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }

    @RequestMapping(value = "/registrarEncuesta/{cltId}", method = RequestMethod.POST)
    public RestResponse registrarEncuesta(@PathVariable Integer cltId, @RequestBody RestResponse encuesta_json) {
        try {
            //save encuesta
            HashMap<String, String> encuestaMap = HelperUtil.fromJSON(encuesta_json.getData().toString(), new TypeReference<HashMap<String, String>>() {
            });

            HashMap<String, String> encuesta = new HashMap<>();
            encuestaMap.forEach((k, v) -> {
                if (k.startsWith("e_")||k.endsWith("_e")) {
                    encuesta.put(k, v);
                }
            });

            Cliente clt = clienteService.getClienteById(cltId);

            clt.setCltEncuesta(HelperUtil.toJSON(encuesta));
            clienteService.updateCliente(clt);

            return new RestResponse(200, true);
        } catch (NumberFormatException | JsonProcessingException | CustomException ex) {
            log.error("error en rest /registrarEncuesta/{cltId}", ex);
            return new RestResponse(500, false);
        }

    }
}
