/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.helper.HelperUtil;
import com.directv.registro.web.sp.service.JbpmService;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dacopan
 */
@ManagedBean(name = "estadisticasBean")
@ViewScoped
public class EstadisticasBean implements Serializable {

    private final Log log = LogFactory.getLog(HelperUtil.class);

    @ManagedProperty(value = "#{JbpmService}")
    private JbpmService jbpmService;

    public EstadisticasBean() {
    }

    @PostConstruct
    public void postConstruct() {
    }

    public String getMemory() {
        return HelperUtil.getMemoryUse();

    }

    public void clearJbpmLog(ActionEvent actionEvent) {
        try {
            jbpmService.clearJbpmLog();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Succesful", "Logs eliminados."));
        } catch (Exception ex) {
            log.error("clear lbpm", ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Error", "Logs no eliminados."));
        }
    }

    public JbpmService getJbpmService() {
        return jbpmService;
    }

    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }

}
