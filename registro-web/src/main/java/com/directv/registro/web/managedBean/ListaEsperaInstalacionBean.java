/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.managedBean;

import com.directv.registro.web.helper.Constants;
import com.directv.registro.web.sp.model.Cliente;
import com.directv.registro.web.sp.model.ListaEsperaInstalacion;
import com.directv.registro.web.sp.service.JbpmService;
import com.directv.registro.web.sp.service.ListaInstalacionService;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author dacopanCM
 */
@ManagedBean(name = "listaInstalacionBean")
@ViewScoped
public class ListaEsperaInstalacionBean implements Serializable {

    private final Log log = LogFactory.getLog(getClass());

    @ManagedProperty(value = "#{ListaInstalacionService}")
    private ListaInstalacionService listaInstalacionService;

    @ManagedProperty(value = "#{JbpmService}")
    private JbpmService jbpmService;

    private List<ListaEsperaInstalacion> listEspera;

    public ListaEsperaInstalacionBean() {
    }

    @PostConstruct
    public void postConstruct() {

    }

    public void atenderInstalacion(ActionEvent actionEvent) {
        ListaEsperaInstalacion q = listaInstalacionService.getFirst();
        if (q != null) {
            Cliente c = q.getCliente();
            //raise signal
            jbpmService.raiseSignal(Long.parseLong(c.getCltProcId()), "equipos_listo_instalacion_signal");
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "JBPM Signal", "Señal recibida con éxito"));
        } else {
        }
    }

    //get set    
    public List<ListaEsperaInstalacion> getListEspera() {
        if (listEspera == null) {
            listEspera = listaInstalacionService.getAll();
        }
        return listEspera;
    }

    public void setListEspera(List<ListaEsperaInstalacion> listEspera) {
        this.listEspera = listEspera;
    }

    //services
    public ListaInstalacionService getListaInstalacionService() {
        return listaInstalacionService;
    }

    public void setListaInstalacionService(ListaInstalacionService listaInstalacionService) {
        this.listaInstalacionService = listaInstalacionService;
    }

    public JbpmService getJbpmService() {
        return jbpmService;
    }

    public void setJbpmService(JbpmService jbpmService) {
        this.jbpmService = jbpmService;
    }

}
