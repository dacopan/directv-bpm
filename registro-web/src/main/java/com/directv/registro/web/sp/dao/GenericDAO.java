/*
 * Copyright (C) 2016 dacopan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.dao;

import java.io.Serializable;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author dacopan
 * @param <T> model to generic DAO
 */
public class GenericDAO<T> implements Serializable {

    @Autowired
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(T eq) {
        getSessionFactory().getCurrentSession().save(eq);
    }

    public void update(T eq) {
        getSessionFactory().getCurrentSession().update(eq);
    }

    public void delete(T eq) {
        getSessionFactory().getCurrentSession().delete(eq);
    }

    public void evict(T eq) {
        getSessionFactory().getCurrentSession().evict(eq);
    }
}
