/*
 * Copyright (C) 2016 dacopanCM
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.directv.registro.web.sp.service;

import com.directv.registro.web.exceptions.CustomException;
import com.directv.registro.web.sp.model.Cliente;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.mail.internet.MimeMessage;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;

/**
 *
 * @author dacopanCM
 */
@Service("EmailService")
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private VelocityEngine velocityEngine;

    @Value("${jom.mail.from}")
    String fromAddress;

    public void notificarCancelarRegistro(Cliente clt, String reason) throws CustomException {
        String subject = "Procesos Subscripción cancelado";
        String content = "Estimado cliente su procesos de suscripción a directv ha sido cancelado:<br/><br/>" + reason;

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    public void notificarDatosInvalidos(Cliente clt, String reason) throws CustomException {
        String subject = "Procesos Subscripción Datos Inválidos";
        String content = "Estimado cliente su procesos de suscripción a directv ha procporcionado datos erróneos<br/>"
                + "Acercarse a firmar un nuevo contrato para proceder a una nueva validación.<br/><br/>" + reason;

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    //facturacion
    public void enviarFactura(Cliente clt, String valorFactura) throws CustomException {
        String subject = "Procesos Subscripción Factura";
        String content = "Estimado cliente su factura por suscripción a direcTV EC tiene un valor de " + clt.getPlan().getPlnPrecio();

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    public void notificarPagonNoValido(Cliente clt, String reason) throws CustomException {
        String subject = "Procesos Subscripción Pago inválido";
        String content = "Estimado cliente su procesos de suscripción a directv ha procporcionado datos erróneos<br/>"
                + "No se ha podido proceder con el cobro o débito.<br/><br/>" + reason;

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    public void notificarSuscripcion(Cliente clt) throws CustomException {
        String subject = "Subscripción exitosa";
        String content = "Estimado cliente su procesos de suscripción a directv ha sido exitoso<br/>";

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);

    }

    //instalacion
    public void notificarFechaInstalacion(Cliente clt, Date lstInsFechaOut) throws CustomException {
        String subject = "Fecha instalación DirecTV";

        LocalDateTime ldt = LocalDateTime.ofInstant(lstInsFechaOut.toInstant(), ZoneId.systemDefault());

        String content = "Estimado cliente un equipo de técnicos de directv se acercará a su domicilio para proceder con la instalación de los equipos<br/>"
                + "en la siguiente fecha: " + ldt.format(DateTimeFormatter.ofPattern("EEEE dd, MMMM yyyy HH:mm"));

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    public void notificarFalloInstalacion(Cliente clt, String reason) throws CustomException {
        String subject = "Instalación Directv fallida";
        String content = "Estimado cliente un equipo de técnicos de directv no ha podido instlar los equipos necesarios en su domicilio";

        senEmail(clt.getCltEmail(), subject, clt.getCltNombre(), content);
    }

    //generic
    @Async
    private void senEmail(String toEmail, String subject, String nombre, String content) {
        MimeMessagePreparator preparator = (MimeMessage mimeMessage) -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage, "UTF-8");
            message.setTo(toEmail);
            message.setFrom(fromAddress); // could be parameterized...
            message.setSubject(subject);

            Map model = new HashMap();

            model.put("nombre", nombre);
            model.put("content", content);

            String text = VelocityEngineUtils.mergeTemplateIntoString(
                    velocityEngine,
                    "mail/directv.min.html", "UTF-8", model);

            mimeMessage.setHeader("Content-Type", "text/plain; charset=UTF-8");
            message.setText(text, true);
        };

        this.mailSender.send(preparator);
    }

}
