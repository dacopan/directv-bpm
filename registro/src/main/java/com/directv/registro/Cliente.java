package com.directv.registro;

/**
 * This class was automatically generated by the data modeler tool.
 */

public class Cliente implements java.io.Serializable {

    static final long serialVersionUID = 1L;

    private Integer cltId;
    private String cltNombre;
    private String cltApellido;
    private String cltCi;
    private String cltTelefono;
    private String cltEmail;
    private String cltDireccion;
    private String cltContrato;
    private Boolean cltInstalado;
    private String cltProcId;
    private String cltEstado;
    private String cltEquipos;
    private String cltEncuesta;
    private Boolean cltIsPrepago;

    public Cliente() {
    }

    public Cliente(Integer cltId, String cltNombre, String cltApellido, String cltCi, String cltTelefono, String cltEmail, String cltDireccion, String cltContrato, Boolean cltInstalado, String cltProcId, String cltEstado, String cltEquipos, String cltEncuesta, Boolean cltIsPrepago) {
        this.cltId = cltId;
        this.cltNombre = cltNombre;
        this.cltApellido = cltApellido;
        this.cltCi = cltCi;
        this.cltTelefono = cltTelefono;
        this.cltEmail = cltEmail;
        this.cltDireccion = cltDireccion;
        this.cltContrato = cltContrato;
        this.cltInstalado = cltInstalado;
        this.cltProcId = cltProcId;
        this.cltEstado = cltEstado;
        this.cltEquipos = cltEquipos;
        this.cltEncuesta = cltEncuesta;
        this.cltIsPrepago = cltIsPrepago;
    }

    public Integer getCltId() {
        return this.cltId;
    }

    public void setCltId(Integer cltId) {
        this.cltId = cltId;
    }


    public String getCltNombre() {
        return this.cltNombre;
    }

    public void setCltNombre(String cltNombre) {
        this.cltNombre = cltNombre;
    }


    public String getCltApellido() {
        return this.cltApellido;
    }

    public void setCltApellido(String cltApellido) {
        this.cltApellido = cltApellido;
    }


    public String getCltCi() {
        return this.cltCi;
    }

    public void setCltCi(String cltCi) {
        this.cltCi = cltCi;
    }


    public String getCltTelefono() {
        return this.cltTelefono;
    }

    public void setCltTelefono(String cltTelefono) {
        this.cltTelefono = cltTelefono;
    }


    public String getCltEmail() {
        return this.cltEmail;
    }

    public void setCltEmail(String cltEmail) {
        this.cltEmail = cltEmail;
    }


    public String getCltDireccion() {
        return this.cltDireccion;
    }

    public void setCltDireccion(String cltDireccion) {
        this.cltDireccion = cltDireccion;
    }


    public String getCltContrato() {
        return this.cltContrato;
    }

    public void setCltContrato(String cltContrato) {
        this.cltContrato = cltContrato;
    }


    public Boolean getCltInstalado() {
        return this.cltInstalado;
    }

    public void setCltInstalado(Boolean cltInstalado) {
        this.cltInstalado = cltInstalado;
    }


    public String getCltProcId() {
        return cltProcId;
    }

    public void setCltProcId(String cltProcId) {
        this.cltProcId = cltProcId;
    }


    public String getCltEstado() {
        return cltEstado;
    }

    public void setCltEstado(String cltEstado) {
        this.cltEstado = cltEstado;
    }


    public Boolean getCltIsPrepago() {
        return cltIsPrepago;
    }

    public void setCltIsPrepago(Boolean cltIsPrepago) {
        this.cltIsPrepago = cltIsPrepago;
    }


    public String getCltEquipos() {
        return cltEquipos;
    }

    public void setCltEquipos(String cltEquipos) {
        this.cltEquipos = cltEquipos;
    }


    public String getCltEncuesta() {
        return cltEncuesta;
    }

    public void setCltEncuesta(String cltEncuesta) {
        this.cltEncuesta = cltEncuesta;
    }


}
