package com.directv.registro.lib;

import com.directv.registro.RestResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.kie.api.runtime.process.ProcessContext;

/**
 * Created by anunaki on 14/07/16.
 */
public class FacturaScript extends GenericScript {


    public FacturaScript(ProcessContext kcontext) {
        super(kcontext, "factura");
    }

    public FacturaScript() {
    }

    /**
     * NO IMPLEMEMTAR
     *
     * @param clt_id
     */
    public void generarFactura(Integer clt_id) {
        System.out.println("dcm: generarFactura");
    }

    public void enviarFactura(Integer clt_id) {
        System.out.println("dcm: enviarFactura");


        WebResource webResource = getWebResource("/enviarFactura/" + clt_id);

        ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);
        System.out.println(response);
    }

    /**
     *
     * @param clt_id client id
     */
    public void notificarPagonNoValido(Integer clt_id) {
        System.out.println("dcm: notificarPagonNoValido");


        WebResource webResource = getWebResource("/notificarPagonNoValido/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("reason").toString()));
        System.out.println(response);
    }

    /**
     *
     * @param clt_id client id
     */
    public void aprobarSuscripcion(Integer clt_id) {
        System.out.println("dcm: aprobarSuscripcion");

        WebResource webResource = getWebResource("/aprobarSuscripcion/" + clt_id);

        ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);
        System.out.println(response);
    }

    /**
     *
     * @param clt_id client id
     */
    public void notificarSuscripcion(Integer clt_id) {
        System.out.println("dcm: notificarSuscripcion");


        WebResource webResource = getWebResource("/notificarSuscripcion/" + clt_id);

        ClientResponse response = webResource.accept("application/json").post(ClientResponse.class);
        System.out.println(response);
    }
}
