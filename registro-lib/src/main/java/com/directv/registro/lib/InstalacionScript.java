package com.directv.registro.lib;

import com.directv.registro.RestResponse;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.kie.api.runtime.process.ProcessContext;

import java.util.HashMap;

/**
 * Created by anunaki on 14/07/16.
 */
public class InstalacionScript extends GenericScript {


    public InstalacionScript(ProcessContext kcontext) {
        super(kcontext, "instalacion");
    }

    /**
     * no implementar
     */
    public void agregarAListaEspera(Integer clt_id) {
        System.out.println("dcm: agregarAListaEspera");

        WebResource webResource = getWebResource("/agregarAListaEspera/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, "cmora"));
        System.out.println(response);
    }

    /**
     * @param clt_id client id
     */
    public void notificarFechaInstalacion(Integer clt_id) {
        System.out.println("dcm: notificarFechaInstalacion");

        WebResource webResource = getWebResource("/notificarFechaInstalacion/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("fecha_instalacion")));
        System.out.println(response);
    }

    /**
     * @param clt_id client id
     */
    public void notificarFalloInstalacion(Integer clt_id) {
        System.out.println("dcm: notificarFalloInstalacion");

        WebResource webResource = getWebResource("/notificarFalloInstalacion/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("reason")));
        System.out.println(response);
    }

    public void eliminarDeListaInstalacion(Integer clt_id) {
        System.out.println("dcm: eliminarDeListaInstalacion");

        WebResource webResource = getWebResource("/eliminarDeListaInstalacion/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class);
        System.out.println(response);
    }

    /**
     * activa equipos al terminar la tarea de usuario
     *
     * @param clt_id client id
     */
    public void activarEquipos(Integer clt_id) {
        System.out.println("dcm: activarEquipos");
        HashMap<String, String> equipos = new HashMap<>();

        equipos.put("antena", kcontext.getVariable("uid_antena").toString());
        equipos.put("control", kcontext.getVariable("uid_control").toString());
        equipos.put("decodificador", kcontext.getVariable("uid_decodificador").toString());

        WebResource webResource = getWebResource("/activarEquipos/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, equipos));
        System.out.println(response);
    }

    public void activarEquiposPrepago(Integer clt_id) {
        System.out.println("dcm: activarEquiposPrepago");
        HashMap<String, String> equipos = new HashMap<>();

        equipos.put("tarjeta", kcontext.getVariable("uid_tarjeta").toString());
        equipos.put("decodificador", kcontext.getVariable("uid_decodificador").toString());

        WebResource webResource = getWebResource("/activarEquipos/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, equipos));
        System.out.println(response);
    }

    /**
     * confirma instalacion al terminar la tarea de usuario
     * NO IMPLEMENTAR
     */
    public void confirmarInstalacion(Integer clt_id) {
        System.out.println("dcm: confirmarInstalacion");
    }

    public void registrarEncuesta(Integer clt_id) {
        System.out.println("dcm: enviarEncuesta");

        WebResource webResource = getWebResource("/registrarEncuesta/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("encuesta_json").toString()));
        System.out.println(response);

    }

}
