package com.directv.registro.lib;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.json.impl.provider.entity.JSONObjectProvider;
import com.sun.jersey.json.impl.provider.entity.JacksonProviderProxy;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.kie.api.runtime.process.ProcessContext;

/**
 * Created by anunaki on 17/07/16.
 */
public class GenericScript {
    protected ProcessContext kcontext;
    protected String registroWebURL;
    protected String service;

    public GenericScript(ProcessContext kcontext, String service) {
        this.kcontext = kcontext;
        registroWebURL = kcontext.getVariable("registro_web_url").toString();
        this.service = service;
    }

    public GenericScript() {
    }

    protected String getRestPath() {
        return registroWebURL + "/api/v1/" + service;
    }

    protected WebResource getWebResource(String path) {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);


        ClientConfig config = new DefaultClientConfig();
        config.getClasses().add(JacksonJaxbJsonProvider.class);
        //config.getClasses().add(JSONObjectProvider.class);
        config.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);

        return Client.create(clientConfig).resource(getRestPath() + path);
    }
}
