package com.directv.registro.lib;

import com.directv.registro.RestResponse;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.runtime.process.ProcessInstance;

import java.util.HashMap;

/**
 * Created by anunaki on 14/07/16.
 */
public class RegistroDatosScript extends GenericScript {


    public RegistroDatosScript(ProcessContext kcontext) {
        super(kcontext, "registro");
    }

    public RegistroDatosScript() {
    }


    public void registrarDatos() {
        long processID = kcontext.getProcessInstance().getId();
        System.out.println("dcm: create process with processID" + processID);
    }

    /**
     * NO IMPLEMENTAR
     */
    public void cancelarRegistro() {
        System.out.println("dcm: cancelarRegistro");
    }

    public void notificarCancelarRegistro(Integer clt_id) {
        System.out.println("dcm: notificar cancelacion registro");

        WebResource webResource = getWebResource("/notificarCancelarRegistro/" + clt_id);

        ClientResponse response = webResource.accept("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("reason").toString()));
        System.out.println(response);
    }

    /**
     * @param clt_id client id
     */
    public void validarDatosContrato(Integer clt_id) {
        System.out.println("dcm: validar datos contrato");

        WebResource webResource = getWebResource("/validardatos/" + clt_id);

        //ClientResponse
        ClientResponse response = webResource.accept("application/json").get(ClientResponse.class);

        if (response.getStatus() != 200) {
            throw new RuntimeException("Failed : HTTP error code : "
                    + response.getStatus());
        }

        //String output = response.getEntity(String.class);
        RestResponse output = response.getEntity(RestResponse.class);

        kcontext.setVariable("clt_contrato_valido", output.getData());
    }

    /**
     * @param clt_id client id
     */
    public void notificarDatosInvalidos(Integer clt_id) {
        System.out.println("dcm: notificarDatosInvalidos");

        WebResource webResource = getWebResource("/notificarDatosInvalidos/" + clt_id);

        ClientResponse response = webResource.accept("application/json").type("application/json").post(ClientResponse.class, new RestResponse(200, kcontext.getVariable("reason").toString()));
        System.out.println(response);
    }

    /**
     *
     */
    public void raiseSignal() {
        ProcessInstance proc = kcontext.getProcessInstance();
        //proc.signalEvent(type, eventObject);

    }

}
