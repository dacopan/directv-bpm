package com.directv.registro;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Created by anunaki on 14/07/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class RestResponse implements java.io.Serializable {

    private int status;
    private Object data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RestResponse() {

    }

    public RestResponse(int status, Object data) {
        this.status = status;
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
