package com.directv.registro.lib;

import com.directv.registro.RestResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.kie.api.runtime.KieRuntime;
import org.kie.api.runtime.process.NodeInstance;
import org.kie.api.runtime.process.ProcessContext;
import org.kie.api.runtime.process.ProcessInstance;

import java.util.HashMap;

/**
 * Created by anunaki on 17/07/16.
 */
public class RestTest {
    public static void main(String[] args) {


        ProcessContext pc = new ProcessContext() {
            @Override
            public ProcessInstance getProcessInstance() {
                return null;
            }

            @Override
            public NodeInstance getNodeInstance() {
                return null;
            }

            @Override
            public Object getVariable(String s) {

                String res = "http://localhost:8180/registro-web";
                switch (s) {
                    case "fecha_instalacion":
                        res = "1469276640000";
                        break;
                    case "uid_antena":
                        res = "23464";
                        break;
                    case "uid_control":
                        res = "2346664";
                        break;
                    case "uid_decodificador":
                        res = "2346884";
                        break;
                    case "encuesta_json":
                        res = "{\"e_consulta\":\"xxx\",\"_fb\":\"org.jbpm.formModeler.components.renderer.FormRenderingComponent\",\"_fp\":\"submitForm\",\"ctxUID\":\"formRenderCtx_651455862_1469286114715\",\"persistForm\":\"false\",\"formRenderCtx_651455862_1469286114715-651455862-clt_id\":\"125\",\"formRenderCtx_651455862_1469286114715-651455862-contratoUrl\":\"498\",\"formRenderCtx_651455862_1469286114715-651455862-encuesta_json\":\"\",\"pAction\":\"\",\"modifiedFieldName\":\"formRenderCtx_651455862_1469286114715-651455862-contratoUrl\"}";
                        break;

                    default:
                        break;
                }
                return res;
            }

            @Override
            public void setVariable(String s, Object o) {
                System.out.println(o);
            }

            @Override
            public KieRuntime getKieRuntime() {
                return null;
            }

            @Override
            public KieRuntime getKnowledgeRuntime() {
                return null;
            }
        };


        int clt_id = 17;
        RegistroDatosScript rds = new RegistroDatosScript(pc);
        //rds.validarDatosContrato(clt_id);
        //rds.notificarDatosInvalidos(clt_id);
        //rds.notificarCancelarRegistro(clt_id);

        FacturaScript fsc = new FacturaScript(pc);
        //fsc.enviarFactura(clt_id);
        //fsc.notificarPagonNoValido(clt_id);
        //fsc.aprobarSuscripcion(clt_id);
        //fsc.notificarSuscripcion(clt_id);
        //fsc.notificarPagonNoValido(clt_id);

        InstalacionScript isc = new InstalacionScript(pc);
        //isc.agregarAListaEspera(clt_id);
        //isc.notificarFechaInstalacion(clt_id);
        //isc.notificarFalloInstalacion(clt_id);
        isc.eliminarDeListaInstalacion(clt_id);
        //isc.activarEquipos(clt_id);
        //isc.activarEquiposPrepago(clt_id);
        //isc.registrarEncuesta(clt_id);

        //testJson(pc);
    }

    private static void testJson(ProcessContext kcontext) {
        try {
            HashMap<String, String> equipos = new HashMap<>();

            equipos.put("antena", kcontext.getVariable("uid_antena").toString());
            equipos.put("control", kcontext.getVariable("uid_control").toString());
            equipos.put("decodificador", kcontext.getVariable("uid_decodificador").toString());

            RestResponse r = new RestResponse(200, equipos);

            System.out.println(r);

            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(r);
            System.out.println(json);
        } catch (Exception ex) {

            System.out.println(ex.getStackTrace());
        }
    }
}
